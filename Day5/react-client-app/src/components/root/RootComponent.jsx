import React, { useState, useCallback } from 'react';
import DataTable from '../common/DataTable';
import employeesApiClient from '../../services/employee-api-client';

const RootComponent = () => {
    const [employees, setEmployees] = useState([]);

    const loadData = useCallback(() => {
        employeesApiClient.getAllEmployees().then(data => {
            setEmployees([...data]);
        });
    }, []); // Empty dependency array because we want `loadData` to remain the same across renders

    return (
        <div className='container text-center'>
            <h2 className='text-info'>React Application</h2>
            <button onClick={loadData} className='btn btn-primary'>Load Data</button>

            <DataTable items={employees}>
                <h4 className="text-primary text-uppercase font-weight-bold">Employees Table</h4>
            </DataTable>
        </div>
    );
}

export default RootComponent;
