const LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport) {
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, email, password, done) {
        // Code to Read Username & Password from database or a file
        // Verify the username and password coming from the client side

        if (email !== 'manish@abc.com')
            return done(null, false, req.flash('loginMessage', 'No user found.'));

        if (password !== 'manish')
            return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

        var user = { email };

        return done(null, user);
    }));
}