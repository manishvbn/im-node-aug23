exports.isAuthenticated = function (req, res, next) {
    // res.status(401).send('Unauthorized Access!');

    // Logic to check if user is authenticated
    if (req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/account');
    }
}

exports.login_get = function (req, res, next) {
    res.render('account/login', { pageTitle: "Login View", message: req.flash('loginMessage') });
}

exports.login_post = function (passport) {
    return passport.authenticate('local-login', {
        successRedirect: '/employees',
        failureRedirect: '/account/login',
        failureFlash: true
    });
}

// -----------------------------------
const jwt = require('jsonwebtoken');
const key = process.env.tokenSecret;

exports.createToken = function (req, res, next) {
    const user = {
        username: req.body.username,
        password: req.body.password
    };

    if (user.username === 'indiamart' && user.password === 'indiamart') {
        const token = jwt.sign({username: user.username}, key, {expiresIn: 3600});
        res.status(200).json({
            success: true,
            message: 'Token Created!',
            token: token
        });
    } else { 
        res.status(403).json({
            success: false,
            message: 'Invalid Credentials!'
        });
    }
}

exports.validateToken = function (req, res, next) {
    var token = req.headers['x-access-token'];

    if (token) {
        jwt.verify(token, key, function (err, decoded) {
            if (err)
                res.status(403).json({
                    success: false,
                    message: 'Invalid Token!'
                });
            else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.status(401).json({
            success: false,
            message: 'No Token Found!'
        });
    }
}