$(document).ready(function () {
    $.post('http://localhost:3000/account/getToken', { username: 'indiamart', password: 'indiamart' },
        function (data, status) {
            if (data.success)
                window.sessionStorage.setItem('token', data.token);
            else
                console.error(data.message);
        });

    $('#btnLoad').click(function () {
        // alert("Button Clicked");

        // Do an AJAX call to get the JSON data and display the HTML
        $('#empTableBody ').empty();

        $.ajax({
            url: 'http://localhost:3000/api/employees',
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                const tmpl = $.templates('#employeeRowTemplate');
                const html = tmpl.render(response.data);
                $('#empTableBody').append(html);

                // $.each(response.data, function (index, item) {
                //     var row = '<tr><td>' + item.id + '</td><td>' + item.name + '</td></tr>';
                //     $('#empTableBody').append(row);
                // });
            }, error: function (err) {
                console.error(err);
            }, beforeSend: function (xhr) {
                xhr.setRequestHeader('x-access-token', window.sessionStorage.getItem('token'));
            }
        });
    });
});