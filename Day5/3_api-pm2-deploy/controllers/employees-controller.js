const path = require('path');

exports.index = async (req, res, next) => {
    res.render("employees/index", { pageTitle: "Employees View" });
}
