const { getEmployees } = require('../data-access/index');
const { EmployeeDTO } = require('../dtos/employee-dto');

exports.getAll = (req, res, next) => {
    getEmployees().then(result => {
        const data = result.map(r => new EmployeeDTO(r._id, r.id, r.name));
        res.status(200).json({ data: data, message: 'Success, all employees fetched' });
    }).catch(eMsg => {
        res.status(500).json({ message: eMsg });
    });
}