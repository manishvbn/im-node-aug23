require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const mongoose = require('mongoose');

const passport = require('passport');
const session = require('express-session');
const flash = require('connect-flash');

require('./config/passport-config')(passport);

mongoose.connect(process.env.mongoUri);

var indexRouter = require('./routes/index');
var employeesRouter = require('./routes/employees');
var employeesApiRouter = require('./routes/employees-api');
var accountRouter = require('./routes/account')(passport);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// app.use(express.static(path.join(__dirname, 'build')));

app.use(session({ secret: process.env.sessionSecret, resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use('/', indexRouter);
app.use('/employees', employeesRouter);
app.use('/api/employees', employeesApiRouter);
app.use('/account', accountRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

mongoose.connection.on('connected', () => {
  console.log(`Connected to MongoDB`);
});

mongoose.connection.on('error', (err) => {
  console.log(`Error connecting to MongoDB - ${err}`);
});

mongoose.connection.on('disconnected', () => {
  console.log(`Disconnected from MongoDB`);
});

module.exports = app;
// aasddas