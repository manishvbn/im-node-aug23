const path = require('path');

exports.index = async (req, res, next) => {
    res.render("employees/index", { pageTitle: "Employees View" });
    // res.sendFile(path.join(process.cwd(), 'build', 'index.html'));
}
