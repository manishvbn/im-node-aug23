exports.isAuthenticated = function (req, res, next) {
    // res.status(401).send('Unauthorized Access!');

    // Logic to check if user is authenticated
    if (req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/account');
    }
}

exports.login_get = function (req, res, next) {
    res.render('account/login', { pageTitle: "Login View", message: req.flash('loginMessage') });
}

exports.login_post = function (passport) { 
    return passport.authenticate('local-login', {
        successRedirect: '/employees',
        failureRedirect: '/account/login',
        failureFlash: true
    });
}