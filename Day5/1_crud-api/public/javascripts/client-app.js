$(document).ready(function () {
    $('#btnLoad').click(function () {
        // alert("Button Clicked");

        // Do an AJAX call to get the JSON data and display the HTML
        $('#empTableBody ').empty();

        $.ajax({
            url: 'http://localhost:3000/api/employees',
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                const tmpl = $.templates('#employeeRowTemplate');
                const html = tmpl.render(response.data);
                $('#empTableBody').append(html);

                // $.each(response.data, function (index, item) {
                //     var row = '<tr><td>' + item.id + '</td><td>' + item.name + '</td></tr>';
                //     $('#empTableBody').append(row);
                // });
            }, error: function (err) {
                console.error(err);
            }
        });
    });
});