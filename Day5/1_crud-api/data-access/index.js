const Employee = require('../models/employee');

exports.getEmployees = async function () {
    try {
        const allEmployees = await Employee.find();
        return allEmployees;
    } catch (err) {
        throw Error(`Could not fetch Employees ${err}`);
    }
}

exports.getEmployee = async function (recordId) {
    try {
        const singleEmployee = await Employee.findById({ _id: recordId });
        return singleEmployee;
    } catch (err) {
        throw Error(`Could not fetch Employee ${err}`);
    }
}

exports.insertEmployee = async function (employee) {
    try {
        const response = await new Employee(employee).save();
        return response;
    } catch (err) {
        throw Error(`Could not create Employee ${err}`);
    }
}

exports.updateEmployee = async function (recordId, employee) {
    try {
        const response = await Employee.findOneAndUpdate({ _id: recordId }, employee);
        return response;
    } catch (err) {
        throw Error(`Could not update Employee ${err}`);
    }
}

exports.deleteEmployee = async function (recordId) {
    try {
        const response = await Employee.findOneAndDelete({ _id: recordId });
        return response;
    } catch (err) {
        throw Error(`Could not delete Employee ${err}`);
    }
}