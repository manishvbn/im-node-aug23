const express = require('express');
const cors = require('cors');

const empApiCtrl = require('../controllers/employees-api-controller');

const router = express.Router();

// router.use(cors());

router.use((cors({
    origin: 'http://localhost:4000'
})));

// GET - /api/employees (Get all employees)
router.get('/', empApiCtrl.getAll);

module.exports = router;