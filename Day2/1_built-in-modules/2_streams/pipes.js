// const fs = require('fs');

// const readStream = fs.createReadStream('./file1.txt', 'utf-8');
// const writeStream = fs.createWriteStream('./file3.txt', 'utf-8');

// readStream.pipe(writeStream);
// console.log("File Copied...");

// -------------------------------------

// const fs = require('fs');
// const zlib = require('zlib');

// fs.createReadStream('./file1.txt')
//     .pipe(zlib.createGzip())
//     .pipe(fs.createWriteStream('./file1.txt.gz'));

// console.log("File Compressed...");

// -------------------------------------

const fs = require('fs');
const { createCipheriv, randomBytes } = require('node:crypto');

const key = 'this-is-super-secret-key';
const algorithm = 'aes-192-cbc';
const iv = randomBytes(16);

fs.createReadStream('./file1.txt')
    .pipe(createCipheriv(algorithm, Buffer.from(key), iv))
    .pipe(fs.createWriteStream('./file1-en.txt'));

console.log("File Encrypted...");