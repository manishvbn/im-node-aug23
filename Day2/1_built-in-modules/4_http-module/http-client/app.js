const https = require('https');
const fs = require('fs');

const url = 'https://jsonplaceholder.typicode.com/posts';
const writeStream = fs.createWriteStream('./posts.json');

const request = https.request(url, { method: 'GET' }, (response) => { 
    if(response.statusCode !== 200) {
        console.error(`Error: ${response.statusCode}`);
        return;
    }

    response.on('data', (data) => {
        console.log("Chunk Received...");
        writeStream.write(data);
    });

    response.on('close', () => {
        console.log("All Data Received...");
        writeStream.close();
    });
});

request.on('error', (error) => {
    console.error(`Error: ${error.message}`);
});

request.end();