const Account = require('./account');
const { emailClient, smsClient } = require('./utilities');

let a1 = new Account(1, 1000, 6);

// Code to diplay message on the console
a1.on('depositSuccess', (balance) => {
    console.log(`\nDeposit successful. Balance is ${balance}`);
});

a1.on('withdrawSuccess', (balance) => {
    console.log(`\nWithdraw successful. Balance is ${balance}`);
});

a1.on('withdrawFailure', (balance) => {
    console.log(`\nWithdraw failed. Balance is ${balance}`);
});

// Code to send email
a1.on('depositSuccess', (balance) => {
    emailClient.send(`Deposit successful. Balance is ${balance}`);
});

a1.on('withdrawSuccess', (balance) => {
    emailClient.send(`Withdraw successful. Balance is ${balance}`);
});

a1.on('withdrawFailure', (balance) => {
    emailClient.send(`Withdraw failed. Balance is ${balance}`);
});

// Code to send sms
a1.on('depositSuccess', (balance) => {
    smsClient.send(`Deposit successful. Balance is ${balance}`);
});

a1.on('withdrawSuccess', (balance) => {
    smsClient.send(`Withdraw successful. Balance is ${balance}`);
});

a1.on('withdrawFailure', (balance) => {
    smsClient.send(`Withdraw failed. Balance is ${balance}`);
});

a1.deposit(1000);
a1.withdraw(2000);
a1.withdraw(1000);