const EventEmitter = require('events');

let instance = null;

class StringEmitter extends EventEmitter {
    constructor() {
        super();
        if (!instance) {
            instance = this;
        }

        this.strArr = ['NodeJS', 'ReactJS', 'AngularJS', 'VueJS', 'EmberJS'];
        this.run();
        return instance;
    }

    run() {
        setInterval(() => {
            var str = this.strArr[Math.floor(Math.random() * this.strArr.length)];
            this.emit('stringEmitted', str);
        }, 2000);
    }

    pushString(cb) {
        setInterval(() => {
            var str = this.strArr[Math.floor(Math.random() * this.strArr.length)];
            cb(str);
        }, 2000);
    }

    getString() {
        var str = this.strArr[Math.floor(Math.random() * this.strArr.length)];
        return str;
    }
}

module.exports = new StringEmitter();