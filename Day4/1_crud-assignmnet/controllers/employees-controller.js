const { getEmployees, getEmployee, updateEmployee, deleteEmployee, insertEmployee } = require("../data-access")

// exports.index = (req, res, next) => {
//     getEmployees().then(employees => {
//         res.render("employees/index", { pageTitle: "Employees View", empList: employees, message: "" });
//     }, eMsg => {
//         res.render("employees/index", { pageTitle: "Employees View", empList: null, message: eMsg });
//     });
// }

exports.index = async (req, res, next) => {
    try {
        const employees = await getEmployees();
        res.render("employees/index", { pageTitle: "Employees View", empList: employees, message: "" });
    }
    catch (eMsg) {
        res.render("employees/index", { pageTitle: "Employees View", empList: null, message: eMsg });
    };
}

exports.details = (req, res, next) => {
    var id = req.params.empid;

    getEmployee(id).then(employee => {
        res.render("employees/details", { pageTitle: "Employee Details View", employee: employee, message: "" });
    }, eMsg => {
        res.render("employees/details", { pageTitle: "Employee Details View", employee: null, message: eMsg });
    });
}

exports.create_get = (req, res, next) => {
    res.render("employees/create", { pageTitle: "Create Employee View", employee: {}, message: "" });
}

exports.create_post = (req, res, next) => {
    var { eid, ename } = req.body;
    var employee = { id: parseInt(eid), name: ename };

    insertEmployee(employee).then(employee => {
        res.redirect('/employees');
    }, eMsg => {
        res.render("employees/create", { pageTitle: "Create Employee View", employee: employee, message: eMsg });
    });
}

exports.edit_get = (req, res, next) => {
    var id = req.params.empid;

    getEmployee(id).then(employee => {
        res.render("employees/edit", { pageTitle: "Employee Edit View", employee: employee, message: "" });
    }, eMsg => {
        res.render("employees/edit", { pageTitle: "Employee Edit View", employee: null, message: eMsg });
    });
}

exports.edit_post = (req, res, next) => {
    var id = req.params.empid;

    var { eid, ename } = req.body;
    var employee = { id: parseInt(eid), name: ename };

    updateEmployee(id, employee).then(employee => {
        res.redirect('/employees');
    }, eMsg => {
        res.render("employees/edit", { pageTitle: "Edit Employee View", employee: employee, message: eMsg });
    });
}

exports.delete_get = (req, res, next) => {
    var id = req.params.empid;

    getEmployee(id).then(employee => {
        res.render("employees/delete", { pageTitle: "Employee Delete View", employee: employee, message: "" });
    }, eMsg => {
        res.render("employees/delete", { pageTitle: "Employee Delete View", employee: null, message: eMsg });
    });
}

exports.delete_post = (req, res, next) => {
    var id = req.params.empid;

    deleteEmployee(id).then(employee => {
        res.redirect('/employees');
    }, eMsg => {
        res.render("employees/delete", { pageTitle: "Employee Delete View", employee: null, message: eMsg });
    });
}