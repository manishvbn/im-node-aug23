// let employees = [{ id: 1, name: "Manish" },
// { id: 2, name: "Abhijeet" },
// { id: 3, name: "Ram" },
// { id: 4, name: "Abhishek" },
// { id: 5, name: "Ramakant" }];

const Employee = require('../models/employee');

let employees = [
    new Employee(1, "Manish"),
    new Employee(2, "Abhijeet"),
    new Employee(3, "Ram"),
    new Employee(4, "Abhishek"),
    new Employee(5, "Ramakant")
];

exports.getAllEmoloyees = () => {
    return employees;
}