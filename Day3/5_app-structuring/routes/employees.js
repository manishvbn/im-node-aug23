const express = require('express');
const router = express.Router();

const da = require('../data-access');

router.get('/', (req, res, next) => {
    res.render('employees/index', { pageTitle: 'Employees View', 'empList': da.getAllEmoloyees() })
});

module.exports = router;