const http = require('http');
const express = require('express');
const logger = require('morgan');
const favicon = require('serve-favicon');

// Create an Express Application Request Handler
const app = express();

app.set('view engine', 'pug');

var employees = [{ id: 1, name: "Manish" },
{ id: 2, name: "Abhijeet" },
{ id: 3, name: "Ram" },
{ id: 4, name: "Abhishek" },
{ id: 5, name: "Ramakant" }];

app.use(logger('dev'));
app.use(favicon(__dirname + "/public/images/favicon.png"));

// app.use((req, res, next) => {
//     console.log('Request - Middleware 1');
//     // res.statusCode = 403;
//     // res.statusMessage = 'Forbidden';
//     // res.end();
//     next();
//     console.log('Response - Middleware 1');
// });

// app.use((req, res, next) => {
//     console.log('Request - Middleware 2');
//     next();
//     console.log('Response - Middleware 2');
// });

// app.use((req, res, next) => {
//     var stTime = new Date().getTime();
//     next();
//     var enTime = new Date().getTime();
//     var tTime = enTime - stTime;
//     console.log(`${req.url} - Total Time: ${tTime} ms`);
// });

// Basic Routes
app.get('/', (req, res, next) => {
    // console.log('Request Handler');
    // throw new Error('Some Error Occured');
    res.render('index', { pageTitle: 'Index View' })
});

// Error Handling Middleware
app.use((err, req, res, next) => {
    console.log('Error Middleware');
    console.log(err);
    res.statusCode = 500;
    res.send("Server Error...");
});

app.get('/about', (req, res, next) => {
    res.render('about', { pageTitle: 'About View' })
});

app.get('/contact', (req, res, next) => {
    res.render('contact', { pageTitle: 'Contact View' })
});

app.get('/employees', (req, res, next) => {
    res.render('employees/index', { pageTitle: 'Employees View', 'empList': employees })
});

// ------------------------------------------- Hosting Code
const server = http.createServer(app);

server.listen(3000);

function onError(err) {
    console.log(err);
}

function onListening(err) {
    var address = server.address();
    console.log("Server started on port: ", address.port);
}

server.on('error', onError);
server.on('listening', onListening);